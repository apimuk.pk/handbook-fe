export interface IProduct {
  _id: string;
  isDelete: boolean;
  name:string;
  quantity:number;
  price:number;
  created_at:Date;
  updated_at:Date;
}

export interface IProductList{
  count: number,
  data: IProduct[],
}

export interface IFormProduct{
  name: string;
  quantity:number;
  price:number;
}