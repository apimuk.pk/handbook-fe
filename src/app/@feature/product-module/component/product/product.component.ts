import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { HttpProductClient } from '../../client/http-product.client';
import { IProduct } from '../../model/product.interface';
import { DialogComponent } from '../dialog/dialog.component';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  public displayedColumns: string[] = ['order','_id','name','quantity','price','created_at','updated_at','action'];
  public dataSource!: MatTableDataSource<IProduct>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  constructor(
    private productClient: HttpProductClient,
    private dialog: MatDialog,
  ) { 
  }

  ngOnInit(): void {
    this.getAllProducts()
  }

  openDialog():void {
    this.dialog.open(DialogComponent, {
      width:'30%',
    }).afterClosed().subscribe(val=>{
      if(val==='save') this.getAllProducts()
    })
  }

  public async getAllProducts():Promise<void>{
    try {
      const response = await this.productClient.getProduct();
      const data = response.data.reduce((prev,current,index)=>{
        prev.push({order: index+1,...current})
        return prev;
      },[] as any)
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } catch (error) {
      console.log(error)
    }
  }

  public async applyFilter(event: Event):Promise<void> {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public async editProduct(row:any):Promise<void>{
    try {
      this.dialog.open(DialogComponent,{
        width:'30%',
        data:row
      }).afterClosed().subscribe(val=>{
        if(val==='update') this.getAllProducts()
      })
    } catch (error) {
      console.log(error)
    }
  }

  public async deleteProduct(id:number):Promise<void>{
    try {
      await this.productClient.removeProduct(id);
      await this.getAllProducts()
      Swal.fire('deleted!',`${id} was deleted!`,'success')
    } catch (error) {
      if(error instanceof HttpErrorResponse){
        Swal.fire('error!',error.statusText,'error')
      }
    }
  }
}
