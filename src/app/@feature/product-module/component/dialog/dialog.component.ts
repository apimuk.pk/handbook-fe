import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { HttpProductClient } from '../../client/http-product.client';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  productForm !: FormGroup;
  actionBtn: string = "Save";
  constructor(
    private formBuilder: FormBuilder,
    private productClient: HttpProductClient,
    @Inject(MAT_DIALOG_DATA) 
    public editData:any,
    private dialogRef: MatDialogRef<DialogComponent>,
  ) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      name:['',Validators.required],
      quantity:['',Validators.required],
      price:['',Validators.required],
    })

    if(this.editData){
      this.actionBtn="Update";
      this.productForm.controls['name'].setValue(this.editData.name);
      this.productForm.controls['quantity'].setValue(this.editData.quantity);
      this.productForm.controls['price'].setValue(this.editData.price);
    }
  }

  public async addProduct():Promise<void>{
    if(this.editData) {
      this.updateProduct()
      return;
    }
    try {
      await this.productClient.addProduct(this.productForm.value);
      this.productForm.reset();
      this.dialogRef.close('save');
      Swal.fire('saved','Products Added!','success')
    } catch (error) {
      if(error instanceof HttpErrorResponse){
        Swal.fire('error!',error.statusText,'error')
      }
    }
  }

  public async updateProduct():Promise<void>{
    try {
      await this.productClient.updateProduct(this.editData._id,this.productForm.value);
      this.productForm.reset();
      this.dialogRef.close('update');
      Swal.fire('Updated',`Products updated!`,'success')
    } catch (error) {
      if(error instanceof HttpErrorResponse){
        Swal.fire('error!',error.statusText,'error')
      }
    }
  }

}
