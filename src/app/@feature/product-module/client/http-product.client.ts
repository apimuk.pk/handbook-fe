import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IFormProduct, IProduct, IProductList } from '../model/product.interface';

@Injectable({
  providedIn: 'root'
})
export class HttpProductClient {

  private baseApi = 'http://localhost:3000/warehouse/product/'

  constructor(
    private http:HttpClient
  ) { }

  public getProduct(): Promise<IProductList> {
    return new Promise<IProductList>((resolve, reject) => {
      this.http.get<IProductList>(this.baseApi).subscribe({
        next: (response) => { 
          resolve(response);
        },
        error: (err) => { reject(err) },
      })
    });
  }

  public addProduct(param:IFormProduct):Promise<IProduct>{
    return new Promise<IProduct>((resolve, reject) => {
      this.http.post<IProduct>(this.baseApi,param).subscribe({
        next: (response) => { 
          resolve(response);
        },
        error: (err) => { reject(err) },
      })
    });
  }

  public updateProduct(id:number,param:IFormProduct):Promise<IProduct>{
    return new Promise<IProduct>((resolve, reject) => {
      this.http.put<IProduct>(this.baseApi+id,param).subscribe({
        next: (response) => { 
          resolve(response);
        },
        error: (err) => { reject(err) },
      })
    });
  }

  public removeProduct(id:number):Promise<IProduct>{
    return new Promise<IProduct>((resolve, reject) => {
      this.http.delete<IProduct>(this.baseApi+id).subscribe({
        next: (response) => { 
          resolve(response);
        },
        error: (err) => { reject(err) },
      })
    });
  }
}
